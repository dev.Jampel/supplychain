#setPATHsoitincludesHLFbinifitexists
if[-d"/workspaces/SupplyChain/supplychain/fabric-samples/bin"];then
    PATH="/workspaces/SupplyChain/supplychain/fabric-samples/bin:$PATH"
fi

chmod -R 0755 ./crypto-config
#Delete existing artifacts
rm -rf ./crypto-config
rm genesis.block mychannel.tx
rm -rf ../../channel-artifacts/*

#GenerateCryptoartifactsfororganizations
cryptogen generate --config=./crypto-config.yaml --output=./crypto-config/